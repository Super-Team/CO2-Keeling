# -*- coding: utf-8 -*-	
from tkinter import *
from tkinter import messagebox, ttk
from PIL import Image, ImageTk
from datetime import datetime
import serial
import serial.tools.list_ports
import matplotlib.pyplot as plt
import requests
import json

root = Tk()
root.title('CO2-Keepling') 
root.geometry('1000x600') 
root.config(bg='#E6E6E6')
root.iconbitmap('img/icon.ico')
root.resizable(0,0)

##FUNCIÓN AUXILIAR PAPA PRUEBAS
def aux():
	print("cosa")

##FUNCIÓN PARA MOSTRAR ALGÚN TIPO DE ERROR
def error(errorString, typerror):
	if(typerror==1):
		messagebox.showerror("Error", errorString)
	if(typerror==2):
		messagebox.showwarning("Advertencia", errorString)
	if(typerror==3):
		messagebox.showinfo("Información", errorString)

##BÚSQUEDA Y VERIFICACIÓN DE LA CONEXIÓN CON EL ARDUINO
serialPort = ""
try:
	ports = list(serial.tools.list_ports.comports())
	for p in ports:
		aux = p
		serialPort = str(aux)[:4]
	ser = serial.Serial(serialPort, 9600, timeout=0)
except serial.SerialException as e:
	root.withdraw()
	error("Verifique la conexión del Arduino.", 1)
	root.destroy()

global String1
String1 = str(datetime.now())

def graph():
	dia = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0]
	nivel = [3, 12, 2, 50, 78, 110, 120]
	plt.plot(dia, nivel)
	plt.title("Contaminación")
	plt.xlabel("Días")
	plt.ylabel("Nivel de contaminación")
	plt.plot(dia, nivel, marker='o', color="r", label='CO2')
	plt.savefig("img\graph.png")

	img = Image.open("img\graph.png")
	img = img.resize((600, 400), Image.ANTIALIAS)
	render = ImageTk.PhotoImage(img)
	charac = Label(root, image=render, bg='#E6E6E6', bd=0)
	charac.image = render
	charac.place(x=380, y=20)
	plt.clf()

send_url = 'http://freegeoip.net/json'
r = requests.get(send_url)
j = json.loads(r.text)
lat = j['latitude']
lon = j['longitude']
ciudad = j['city']
estado = j['region_name']
pais = j['country_name']
aux_ = ciudad + ", " + estado + ", " + pais

entry1 = Entry(root, bd=0, textvariable=String1, font=('Verdana', 8), justify='left', state="disable")
entry1.grid()
entry1.place(x=190, y=50)
entry1.config(state="normal")
entry1.insert(0, String1)
entry1.config(state="disable")
entry1.config(disabledbackground="#E6E6E6", disabledforeground="#000")

L0 = Label(root, bg='#E6E6E6', font=("Verdana", 8, "bold"), fg='#000', justify=LEFT, text="Ubicación: ").place(x=10, y=20)
L1 = Label(root, bg='#E6E6E6', font=("Verdana", 8, "bold"), fg='#000', justify=LEFT, text="Fecha y hora: ").place(x=10, y=50)
L3 = Label(root, bg='#E6E6E6', font=("Verdana", 8, "bold"), fg='#000', justify=LEFT, text="Nivel de contaminación: ").place(x=10, y=80)

Lubi = Label(root, bg='#E6E6E6', font=("Verdana", 8), fg='#000', justify=LEFT, text=aux_).place(x=190, y=20)
level = Label(root, bg='#E6E6E6', font=("Verdana", 8), fg='#000', justify=LEFT, text="0.0 %").place(x=190, y=80)

img = Image.open("img\output.png")
img = img.resize((600, 400), Image.ANTIALIAS)
render = ImageTk.PhotoImage(img)
charac = Label(root, image=render, bg='#E6E6E6', bd=0)
charac.image = render
charac.place(x=380, y=20)

def update():
	global String1
	String1 = str(datetime.now())
	entry1.config(state="normal")
	entry1.insert(0, String1)
	entry1.config(state="disable")
	

def infoSoftware():
	error("Software desarrollado bajo Licencia MIT.\nCopyright (c) 2017, CO2-Keeling\n\n"+
	"Desarrollado por:\nMauricio Reyes, Ernesto Ruíz, César Rivera, y Azael Rodríguez.\n\n"+
	"Tecnológico Superior de Jalisco (Zapopan).",
	3)

btnUpdate = Button(root, text="Actualizar", font=('Verdana', 8), width=20, command=update).place(x=10, y=120)

##CREACIÓN DE MENÚS
menubar = Menu(root)

##MENÚ DE ARCHIVO
filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="Verificar conexión", command=aux)
filemenu.add_command(label="Guardar", command=aux)
filemenu.add_command(label="Actualizar", command=aux)
filemenu.add_command(label="Graficar", command=graph)
filemenu.add_command(label="Imprimir", command=aux)
filemenu.add_separator()
filemenu.add_command(label="Salir", command=root.quit)
menubar.add_cascade(label="Archivo", menu=filemenu)

##MENÚ DE AYUDA
helpmenu = Menu(menubar, tearoff=0)
helpmenu.add_command(label="Ver ayuda", command=aux)
helpmenu.add_separator()
helpmenu.add_command(label="Información de software", command=infoSoftware)
menubar.add_cascade(label="Ayuda", menu=helpmenu)

root.config(menu=menubar)
root.mainloop()

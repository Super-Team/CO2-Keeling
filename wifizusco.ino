
/*
Mauricio Reyes 09/12/2016
 */


#include <SPI.h>
#include <WiFi.h>

char Red[] = "NombreRed" ; // WIFI a Conectar
char Pass[] = "secretPassword" ;    // Password WPA, WPA2, O vergas nazis

int status = WL_IDLE_STATUS ; //Función de ESP8266
char Servidor[] = "www.google.com" ;    //Direccion de HOST

WiFiClient Cliente; //Librería de Cliente ESP8266

void setup() 
{
  Serial.begin(9600) ; //Conectividad Serial
  while (!Serial) 
  {
    ; // Aguarda hasta que la conexion este vinculada
  }

  // Si se encuentra Conectado el ESP8266
  if (WiFi.status() == WL_NO_SHIELD) 
  {
    Serial.println("No se encuentra ESP8266") ;
    while (true) ;
  }

/*
 * IMPORTANTE: ACTUALIZAR EL FIRMWARE AL INDICADO EN LA SIGUIENTE FUNCIÓN
 * SE CERCIORA CON COMANDOS AT
 */
  String fv = WiFi.firmwareVersion() ;
  if (fv != "1.1.0") 
  {
    Serial.println("Firmware Desactualizado") ;
  }

  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) 
  {
    Serial.print("Conectando a ") ;
    Serial.println(ssid) ;
    // Ciclo de Vinculación a Red WIFI
    status = WiFi.begin(ssid, pass) ;

    // 10 segundos de comprobar Conexión
    delay(10000) ;
  }
  Serial.println("Conectado con Éxito a la red") ;
  Statusdelwifi() ;

  Serial.println("\nVinculando Conexión al Server...") ;
  // Modificar PUERTO de SAN BLAS
  if (client.connect(server, 80)) 
  {
    Serial.println("Vinculación con Servidor Exitosa") ;
    // SOLICITUD HTTP GET (CAMBIAR FORMATO SEGÚN LA SOLICITUD
    //Conexión de Prueba a Google con solicitud GET
    client.println("GET /search?q=arduino HTTP/1.1") ;
    client.println("Host: www.google.com") ;
    client.println("Connection: close") ;
    client.println() ;
  }
}

void loop() 
{
/*
 * Retorno de Carro para la conexión al Servidor
 */
  while (client.available()) 
  {
    char c = client.read() ;
    Serial.write(c) ;
  }

  // Si se detiene la conexión del servidor por PENE RAZÓN
  if (!client.connected()) 
  {
    Serial.println() ;
    Serial.println("Se desconecto la conexión del servidor.") ;
    client.stop() ;

    //NO HAGAS NADA
    while (true); //MAURICIO 12/25/2016
  }
}


void Statusdelwifi() 
{
  // Recuerda la conexión del Wifi vinculado, SI, ESTA MIERDA GUARDA LA CONEXIÓN!! 11/05/2017 MAURICIO 
  Serial.print("RED WIFI RECORDADA: ") ;
  Serial.println(WiFi.SSID()) ;

  // BRINDA IP DEL ESP8266 QUE GUAPO SOY
  IPAddress ip = WiFi.localIP() ;
  Serial.print("IP ESP8266: ") ;
  Serial.println(ip) ;

  // Mide la Intensidad de la conexión WIFI:
  long rssi = WiFi.RSSI() ;
  Serial.print("La intensidad es:") ;
  Serial.print(rssi) ;
  //EN BAUDIOS PAPA 7u7
  Serial.println("dBm") ;
}

//MI PAPA ES HOMBRE & MI MAMÁ ES MUJER
